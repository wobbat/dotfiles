;;; -*- no-byte-compile: t -*-
(define-package "tern" "20181108.722" "Tern-powered JavaScript integration" '((json "1.2") (cl-lib "0.5") (emacs "24")) :commit "4ba411719279c62d9c0acd1243a03477ada1ac32" :authors '(("Marijn Haverbeke")) :maintainer '("Marijn Haverbeke") :url "http://ternjs.net/")
