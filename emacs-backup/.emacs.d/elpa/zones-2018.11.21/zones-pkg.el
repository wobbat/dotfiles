;;; -*- no-byte-compile: t -*-
(define-package "zones" "2018.11.21" "Zones of text - like multiple regions" 'nil :url "https://elpa.gnu.org/packages/zones.html" :keywords '("narrow" "restriction" "widen" "region" "zone"))
