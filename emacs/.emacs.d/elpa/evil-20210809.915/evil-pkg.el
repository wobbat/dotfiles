(define-package "evil" "20210809.915" "Extensible Vi layer for Emacs."
  '((emacs "24.1")
    (goto-chg "1.6")
    (cl-lib "0.5"))
  :commit "6439c91739ff919a4699202ca92890a47d517d2b" :keywords
  '("emulation" "vim")
  :url "https://github.com/emacs-evil/evil")
;; Local Variables:
;; no-byte-compile: t
;; End:
