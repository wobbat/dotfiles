(define-package "orderless" "20210812.2035" "Completion style for matching regexps in any order"
  '((emacs "26.1"))
  :commit "1a7011ac9c476dbb083c5ead88462a5f520ef8aa" :authors
  '(("Omar Antolín Camarena" . "omar@matem.unam.mx"))
  :maintainer
  '("Omar Antolín Camarena" . "omar@matem.unam.mx")
  :keywords
  '("extensions")
  :url "https://github.com/oantolin/orderless")
;; Local Variables:
;; no-byte-compile: t
;; End:
