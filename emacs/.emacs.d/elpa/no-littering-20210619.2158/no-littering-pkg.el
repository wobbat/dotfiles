;;; Generated package description from no-littering.el  -*- no-byte-compile: t -*-
(define-package "no-littering" "20210619.2158" "help keeping ~/.emacs.d clean" '((cl-lib "0.5")) :commit "b12a85a5afff7b5d60f889c1c2e8f5deab7fdbae" :authors '(("Jonas Bernoulli" . "jonas@bernoul.li")) :maintainer '("Jonas Bernoulli" . "jonas@bernoul.li") :url "https://github.com/emacscollective/no-littering")
