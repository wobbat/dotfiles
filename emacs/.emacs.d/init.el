(use-package org)

(org-babel-load-file "~/.emacs.d/config.org")
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(gruvbox-theme use-package no-littering auto-package-update)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(avy-lead-face ((t (:foreground "#ffaf00"))))
 '(avy-lead-face-0 ((t (:foreground "#87af87"))))
 '(avy-lead-face-2 ((t (:foreground "#d3869b"))))
 '(org-level-1 ((t (:weight bold :font "Consolas" :height 1.5 :foreground "#ffaf00"))))
 '(org-level-2 ((t (:weight bold :height 1.25 :foreground "#87af87"))))
 '(org-level-3 ((t (:height 1.15))))
 '(vertico-current ((t (:background "#3a3f5a")))))
